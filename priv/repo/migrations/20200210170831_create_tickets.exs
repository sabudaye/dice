defmodule NaiveDice.Repo.Migrations.CreateTickets do
  use Ecto.Migration

  def change do
    create table(:tickets, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :user_name, :string
      add :event_id, :integer
      add :state, :string, default: "pending"
      add :intent, :string

      timestamps()
    end

    create(index(:tickets, [:event_id]))
    create(unique_index(:tickets, [:event_id, :user_name], name: :tickets_event_id_user_name))
    create(unique_index(:tickets, [:intent], name: :tickets_intent))
  end
end
