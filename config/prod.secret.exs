# In this file, we load production configuration and secrets
# from environment variables. You can also hardcode secrets,
# although such is generally not recommended and you have to
# remember to add this file to your .gitignore.
use Mix.Config

database_url =
  System.get_env("DATABASE_URL") ||
    raise """
    environment variable DATABASE_URL is missing.
    For example: ecto://USER:PASS@HOST/DATABASE
    """

config :naive_dice, NaiveDice.Repo,
  ssl: true,
  url: database_url,
  pool_size: String.to_integer(System.get_env("POOL_SIZE") || "10")

secret_key_base =
  System.get_env("SECRET_KEY_BASE") ||
    raise """
    environment variable SECRET_KEY_BASE is missing.
    You can generate one by calling: mix phx.gen.secret
    """

config :naive_dice, NaiveDiceWeb.Endpoint,
  http: [:inet6, port: String.to_integer(System.get_env("PORT") || "4000")],
  secret_key_base: secret_key_base

publishable_key =
  System.get_env("PUBLISHABLE_KEY") ||
    raise """
    environment variable PUBLISHABLE_KEY is missing.
    You can generate one by calling: mix phx.gen.secret
    """

config :naive_dice, publishable_key: publishable_key

stripe_webhook_secret =
  System.get_env("STRIPE_WEBHOOK_SECRET") ||
    raise """
    environment variable STRIPE_WEBHOOK_SECRET is missing.
    You can generate one by calling: mix phx.gen.secret
    """

config :naive_dice, wh_secret: stripe_webhook_secret

stripe_api_key =
  System.get_env("STRIPE_API_KEY") ||
    raise """
    environment variable STRIPE_API_KEY is missing.
    You can generate one by calling: mix phx.gen.secret
    """

config :stripity_stripe, api_key: stripe_api_key

# ## Using releases (Elixir v1.9+)
#
# If you are doing OTP releases, you need to instruct Phoenix
# to start each relevant endpoint:
#
#     config :naive_dice, NaiveDiceWeb.Endpoint, server: true
#
# Then you can assemble a release by calling `mix release`.
# See `mix help release` for more information.
