# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :naive_dice,
  ecto_repos: [NaiveDice.Repo]

config :naive_dice, publishable_key: "pk_test_YZpSiWNP4T7PJL7m5eMBfoUY00QXUdVaAg"
config :naive_dice, wh_secret: "whsec_eWf4Bt3sq4bmgFjK69OeLmGQloGiD5tg"

config :stripity_stripe, api_key: "sk_test_tlcnNdUXLihxuzws1OlEqepm009PAn4Smm"

config :stripity_stripe, :retries, [max_attempts: 3, base_backoff: 500, max_backoff: 2_000]

# Configures the endpoint
config :naive_dice, NaiveDiceWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "JvdAK76n53ApcZVDT3awEijcXOUVcwAxDG5Qza3KduNqVbGZhV2rb8PT9u4mq4zw",
  render_errors: [view: NaiveDiceWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: NaiveDice.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
