# NaiveDice

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create, migrate and seed your database with `mix ecto.setup`
  * Install Node.js dependencies with `cd assets && npm install`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

## Notes about implementation

### Reliability

Current implementation strongly relies on database and Stripe to be available,
in case of their outage it wouldn't be possible to make reservations and payments.
There are ways to workaround these issues.

One of possible solution for database is to implement Local-first software approach with
local storage on client side and with event-based communication between client and server,
same goes to interaction between application and database,
application must keep track of events and sync them with database and
other nodes in a cluster. But that's too much of effort in implementation of
synchronisation algorithms.

Other way is to make database more reliable, make database-native replicated cluster,
put it behind of set of load balancers with IP-failover. Requires hardware and poeple to configure
and maintain infrastructure.

Kubernetes in Google Cloud and back-up in AWS (or other way around) is also possible way to go.
Requires people to maintain cloud infrastructure, in a long term it is more expensive then
self-hosted solutions but probably more reliable.

Stripe is required to make payments, because of synchronous requests to Stripe
to get Payment Intents (from server) and their confirmation (from client).
The way to work around it - make our own forms for credit cards (what about other payment systems?),
store clients personal data on our side and communicate with Stripe through API,
keeping track of requests and retry them in case of failure using Stripe's idempotent requests.
But that's too much of effort again, and creates a lot of issues including legal ones.

### Correctness

- there is only two states for a ticket - `pending` and `purchased`
- each event ticket can be created only once, which simplifies limiting
- if ticket reservation expired (based on `updated_at` field) same ticket will be offered to next client
- payment confirmation is based only on `charge.suceeded` Stripe webhook

Combination of these factors creates an issue that in case of our outage or Stripe outage
or network issue we might not receive `charge.suceeded` event and Stripe will retry it only once an hour,
which means that already sold ticket might be offered to some other custumer.
It might be fixed in this way:

- change `pending` state to `intent_created` for Stripe's `payment_intent.created` event
- add `intent_confirmed` for Stripe's `payment_intent.confirmed` event
- never change `user_name` for tickets
- add `expired` field for ticket, `false` by default
- not `expired` ticket in `intent_created` state must be offered to returned user otherwise new ticket must be created
- make globaly registered process, which should every minute trigger expiration logic for tickets in `intent_created` and with `created_at` <= 5 minutes ago
- make a process on every node which should be able to register ticket and actively trigger expiration logic per registered ticket after 5 minute timeout
- in expiration logic check Stripe API for charges before setting ticket as `expired` in database, handle situations when there are charges
- never expire tickets in `intent_confirmed` and `purchased` state
- count reserved tickets as not `expired` ones
- rate limit amount of tickets created in a second/minute, per user and in total, response with `429` when limit is hit
- handle situation when Stipe event received for `expired` ticket
- clear `expired` tickets from database after 3 days (Stripe's production limit for webhooks retry), log this event

Another issue is lack of implementation for refunds and canceled events/tickets.
And another one is that application doesn't work without JS and redirection after payment is done
by changing `window.location.href`. There is aslo no validation for user inputs.

### Performance & Optimisations

Current implementation is not optimal:
- it makes redundant database queries per user action
- concurrency and limiting is done using `:global.trans`, which isn't optimal there are faster scalable alternatives pg2, gproc, syn, lasp, but transactions must be implemented manually

## Heroku Deployment

This app is prepared for heroku deployment.
Please check [the deployment guides](https://hexdocs.pm/phoenix/heroku.html).

TL;DR:
```
#/bin/bash
heroku create --buildpack hashnuke/elixir
heroku buildpacks:add https://github.com/gjaldon/heroku-buildpack-phoenix-static.git
heroku addons:create heroku-postgresql
heroku config:set SECRET_KEY_BASE=`mix phx.gen.secret`

other variables to be set are listed in config/prod.secret.exs

git push heroku master
heroku run mix ecto.migrate
heroku run mix run priv/repo/seeds.exs
heroku open
```

Set all other env variables you need (e.g. Stripe key) via `heroku config:set`

https://arcane-escarpment-54053.herokuapp.com/
