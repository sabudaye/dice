defmodule NaiveDiceWeb.TicketControllerTest do
  use NaiveDiceWeb.ConnCase, async: true
  alias NaiveDice.Events
  alias NaiveDice.Events.Ticket
  alias NaiveDice.Repo

  import NaiveDiceWeb.Router.Helpers, only: [event_ticket_path: 3, ticket_path: 3]

  setup do
    event = NaiveDice.Repo.insert!(%NaiveDice.Events.Event{title: "title", allocation: 5})
    {:ok, event: event}
  end

  test "GET /events/:event_id/tickets/new", %{conn: conn, event: event} do
    conn = get(conn, event_ticket_path(conn, :new, event.id))
    assert html_response(conn, 200) =~ "John Doe"
  end

  test "POST /events/:event_id/tickets", %{conn: conn, event: event} do
    params = %{"ticket" => %{"user_name" => "test_create"}}
    conn = post(conn, event_ticket_path(conn, :create, event.id), params)
    ticket = Repo.get_by(Ticket, event_id: event.id, user_name: "test_create")

    assert ticket != nil
    assert redirected_to(conn) =~ ticket_path(conn, :edit, ticket.id)
  end

  test "GET /tickets/:id/edit", %{conn: conn, event: event} do
    {:ok, ticket} = Events.reserve_ticket(event, "test_edit")
    conn = get(conn, ticket_path(conn, :edit, ticket.id))

    assert html_response(conn, 200) =~ "test_edit"
  end

  test "GET /tickets/:id", %{conn: conn, event: event} do
    {:ok, ticket} = Events.reserve_ticket(event, "test_show")
    conn = get(conn, ticket_path(conn, :show, ticket.id))

    assert html_response(conn, 200) =~ "test_show"
  end
end
