defmodule NaiveDiceWeb.GuestControllerTest do
  use NaiveDiceWeb.ConnCase, async: true

  alias NaiveDice.Events

  import NaiveDiceWeb.Router.Helpers, only: [event_guest_path: 3]

  setup do
    event = NaiveDice.Repo.insert!(%NaiveDice.Events.Event{title: "title", allocation: 5})
    {:ok, event: event}
  end

  test "GET /events/:event_id/guests/", %{conn: conn, event: event} do
    {:ok, _ticket} = Events.reserve_ticket(event, "test_guests_get")

    conn = get(conn, event_guest_path(conn, :index, event.id))
    assert html_response(conn, 200) =~ "test_guests_get"
  end

  test "DELETE /events/:event_id/guests/reset", %{conn: conn,  event: event} do
    {:ok, _ticket} = Events.reserve_ticket(event, "test_guests_delete")

    conn = delete(conn, event_guest_path(conn, :reset_guests, event.id))
    assert redirected_to(conn) =~ "/"
  end
end
