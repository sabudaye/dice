defmodule NaiveDiceWeb.PaymentControllerrTest do
  use NaiveDiceWeb.ConnCase, async: true

  @secret Application.get_env(:naive_dice, :wh_secret)

  setup do
    event =
      NaiveDice.Repo.insert!(%NaiveDice.Events.Event{title: "title", allocation: 5, price: 500})

    {:ok, event: event}
  end

  test "POST /payment/intent returns valid response", %{conn: conn, event: event} do
    {:ok, ticket} = NaiveDice.Events.reserve_ticket(event, "test_payment_intent")
    conn = post(conn, "/payment/intent", %{id: ticket.id})
    assert %{"clientSecret" => _, "id" => _, "publishableKey" => _} = json_response(conn, 200)
  end

  test "POST /payment/webhook changes state of ticket to purchased", %{conn: conn, event: event} do
    {:ok, ticket} = NaiveDice.Events.reserve_ticket(event, "test_payment_webhook")

    {:ok, _ticket} =
      NaiveDice.Events.set_ticket_intent(ticket, %{id: "pi_1GIt9OIa482UCtYjHQkrJGp6"})

    timestamp = System.system_time(:second)
    payload = webhook_charge_succeeded_body()
    signature = generate_signature(timestamp, payload)
    signature_header = create_signature_header(timestamp, "v1", signature)

    conn =
      conn
      |> put_req_header("stripe-signature", signature_header)
      |> put_req_header("content-type", "application/json")
      |> post("/payment/webhook", webhook_charge_succeeded_body())

    assert %{"status" => "success"} = json_response(conn, 200)

    {:ok, ticket} = NaiveDice.Events.get_ticket_by_id(ticket.id)

    assert ticket.state == "purchased"
  end

  defp webhook_charge_succeeded_body() do
    "{\n  \"id\": \"evt_1GIt9WIa482UCtYji67M1gyZ\",\n  \"object\": \"event\",\n  \"api_version\": \"2019-12-03\",\n  \"created\": 1583312670,\n  \"data\": {\n    \"object\": {\n      \"id\": \"ch_1GIt9VIa482UCtYjyJ5ErdTh\",\n      \"object\": \"charge\",\n      \"amount\": 500,\n      \"amount_refunded\": 0,\n      \"application\": null,\n      \"application_fee\": null,\n      \"application_fee_amount\": null,\n      \"balance_transaction\": \"txn_1GIt9WIa482UCtYjl0p074qL\",\n      \"billing_details\": {\n        \"address\": {\n          \"city\": null,\n          \"country\": null,\n          \"line1\": null,\n          \"line2\": null,\n          \"postal_code\": \"42424\",\n          \"state\": null\n        },\n        \"email\": null,\n        \"name\": null,\n        \"phone\": null\n      },\n      \"captured\": true,\n      \"created\": 1583312669,\n      \"currency\": \"usd\",\n      \"customer\": null,\n      \"description\": null,\n      \"destination\": null,\n      \"dispute\": null,\n      \"disputed\": false,\n      \"failure_code\": null,\n      \"failure_message\": null,\n      \"fraud_details\": {\n      },\n      \"invoice\": null,\n      \"livemode\": false,\n      \"metadata\": {\n      },\n      \"on_behalf_of\": null,\n      \"order\": null,\n      \"outcome\": {\n        \"network_status\": \"approved_by_network\",\n        \"reason\": null,\n        \"risk_level\": \"normal\",\n        \"risk_score\": 2,\n        \"seller_message\": \"Payment complete.\",\n        \"type\": \"authorized\"\n      },\n      \"paid\": true,\n      \"payment_intent\": \"pi_1GIt9OIa482UCtYjHQkrJGp6\",\n      \"payment_method\": \"pm_1GIt9VIa482UCtYj9A1xBidT\",\n      \"payment_method_details\": {\n        \"card\": {\n          \"brand\": \"visa\",\n          \"checks\": {\n            \"address_line1_check\": null,\n            \"address_postal_code_check\": \"pass\",\n            \"cvc_check\": \"pass\"\n          },\n          \"country\": \"US\",\n          \"exp_month\": 4,\n          \"exp_year\": 2024,\n          \"fingerprint\": \"4RGm82a2JSXsj5FI\",\n          \"funding\": \"credit\",\n          \"installments\": null,\n          \"last4\": \"4242\",\n          \"network\": \"visa\",\n          \"three_d_secure\": null,\n          \"wallet\": null\n        },\n        \"type\": \"card\"\n      },\n      \"receipt_email\": null,\n      \"receipt_number\": null,\n      \"receipt_url\": \"https://pay.stripe.com/receipts/acct_1GHvmPIa482UCtYj/ch_1GIt9VIa482UCtYjyJ5ErdTh/rcpt_GqaKJzZcy3CGKoI0KuxbhVLGsPnRoMb\",\n      \"refunded\": false,\n      \"refunds\": {\n        \"object\": \"list\",\n        \"data\": [\n\n        ],\n        \"has_more\": false,\n        \"total_count\": 0,\n        \"url\": \"/v1/charges/ch_1GIt9VIa482UCtYjyJ5ErdTh/refunds\"\n      },\n      \"review\": null,\n      \"shipping\": null,\n      \"source\": null,\n      \"source_transfer\": null,\n      \"statement_descriptor\": null,\n      \"statement_descriptor_suffix\": null,\n      \"status\": \"succeeded\",\n      \"transfer_data\": null,\n      \"transfer_group\": null\n    }\n  },\n  \"livemode\": false,\n  \"pending_webhooks\": 2,\n  \"request\": {\n    \"id\": \"req_DlDNoZ859icDcn\",\n    \"idempotency_key\": null\n  },\n  \"type\": \"charge.succeeded\"\n}"
  end

  defp generate_signature(timestamp, payload, secret \\ @secret) do
    :crypto.hmac(:sha256, secret, "#{timestamp}.#{payload}")
    |> Base.encode16(case: :lower)
  end

  defp create_signature_header(timestamp, scheme, signature) do
    "t=#{timestamp},#{scheme}=#{signature}"
  end
end
