defmodule NaiveDiceWeb.EventControllerTest do
  use NaiveDiceWeb.ConnCase, async: true

  setup do
    event = NaiveDice.Repo.insert!(%NaiveDice.Events.Event{title: "title", allocation: 5})
    {:ok, event: event}
  end

  test "GET /events/", %{conn: conn, event: event} do
    conn = get(conn, "/events")
    assert html_response(conn, 200) =~ event.title
  end

  test "GET /", %{conn: conn, event: event} do
    conn = get(conn, "/")
    assert html_response(conn, 200) =~ event.title
  end

  test "GET /events/:id", %{conn: conn, event: event} do
    conn = get(conn, "/events/" <> Integer.to_string(event.id))
    assert html_response(conn, 200) =~ event.title
  end

  test "GET /events/:id Not found", %{conn: conn, event: event} do
    conn = get(conn, "/events/" <> Integer.to_string(event.id + 1))
    assert html_response(conn, 404) =~ "404"
  end
end
