defmodule NaiveDice.EventsTest do
  use ExUnit.Case, async: true

  alias NaiveDice.Events
  alias NaiveDice.Events.{Event, Ticket}

  setup do
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(NaiveDice.Repo)

    event = NaiveDice.Repo.insert!(%Event{title: "events_test", allocation: 5})
    {:ok, event: event}
  end

  test "change_ticket_owner/2 sets new user_name to ticket", %{event: event} do
    assert {:ok, ticket1} =
             %Ticket{}
             |> Ticket.changeset(%{
               event_id: event.id,
               user_name: "test_change_ticket_owner",
               state: "pending"
             })
             |> NaiveDice.Repo.insert()

    assert {:ok, ticket2} = Events.change_ticket_owner(ticket1, "test_change_ticket_owner2")
    assert %Ticket{user_name: "test_change_ticket_owner2"} = ticket2
  end

  test "reserve_ticket/2 doesn't allow to reserve the same ticket twice", %{event: event} do
    assert {:ok, %Ticket{} = ticket} = Events.reserve_ticket(event, "test_reserve_twice")
    assert {:error, changeset} = Events.reserve_ticket(event, "test_reserve_twice")
    assert [user_name: {"ticket already exists", _constraint}] = changeset.errors
  end

  test "reserve_ticket/2 doesn't allow to reserve more tickets then allocated in event", %{event: event} do
    assert {:ok, 0} == Events.tickets_sold(event)

    1..10
    |> Enum.map(
      &Task.async(fn ->
        Events.reserve_ticket(event, "test_reserve#{&1}")
      end)
    )
    |> Enum.map(&Task.await(&1, :infinity))

    assert {:ok, 5} == Events.tickets_sold(event)
  end

  test "tickets_sold/1 returns amount of tickets created for an event", %{event: event} do
    assert {:ok, 0} == Events.tickets_sold(event)
    assert {:ok, %Ticket{}} = Events.reserve_ticket(event, "test_tickets_sold")
    assert {:ok, 1} == Events.tickets_sold(event)
  end

  test "all_guests/1 returns list of user_names from tickets of an event", %{event: event} do
    assert {:ok, %Ticket{}} = Events.reserve_ticket(event, "test_all_guests1")
    assert {:ok, %Ticket{}} = Events.reserve_ticket(event, "test_all_guests2")
    assert {:ok, ["test_all_guests1", "test_all_guests2"]} == Events.all_guests(event)
  end

  test "delete_all_tickets/1 dele", %{event: event} do
    assert {:ok, %Ticket{}} = Events.reserve_ticket(event, "test_delete_all1")
    assert {:ok, %Ticket{}} = Events.reserve_ticket(event, "test_delete_all2")
    assert {:ok, 2} == Events.tickets_sold(event)
    assert {:ok, :removed} = Events.delete_all_tickets(event)
    assert {:ok, 0} == Events.tickets_sold(event)
  end
end
