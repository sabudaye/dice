defmodule NaiveDiceWeb.CacheBodyReader do
  @moduledoc """
  Moudle to store request raw body in conn before parsing
  required for webhooks validations
  read more
  https://dashboard.stripe.com/test/webhooks
  https://github.com/phoenixframework/phoenix/issues/459
  """
  def read_body(conn, opts) do
    {:ok, body, conn} = Plug.Conn.read_body(conn, opts)
    conn = Plug.Conn.put_private(conn, :raw_body, body)
    {:ok, body, conn}
  end

  def read_cached_body(conn) do
    conn.private[:raw_body]
  end
end
