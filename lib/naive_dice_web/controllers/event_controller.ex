defmodule NaiveDiceWeb.EventController do
  use NaiveDiceWeb, :controller
  action_fallback NaiveDiceWeb.FallbackController

  alias NaiveDice.Events

  @spec index(Plug.Conn.t(), any) :: Plug.Conn.t()
  def index(conn, _params) do
    events = Events.list_all()

    render(conn, "index.html", %{events: events})
  end

  @spec show(Plug.Conn.t(), %{required(String.t()) => pos_integer()}) ::
          {:error, :not_found} | Plug.Conn.t()
  def show(conn, %{"id" => id}) do
    with {:ok, event} <- Events.get_event_by_id(id) do
      render(conn, "show.html", %{event: event})
    end
  end
end
