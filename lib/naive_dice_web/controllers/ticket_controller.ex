defmodule NaiveDiceWeb.TicketController do
  use NaiveDiceWeb, :controller
  alias NaiveDice.Events

  require Logger

  action_fallback(NaiveDiceWeb.FallbackController)

  # STEPS OF THE WIZARD

  @doc """
  STEP 1: Renders an empty form with user name input
  That's an entry point for the booking flow.
  """
  def new(conn, %{"event_id" => event_id}) do
    with {:ok, event} <- Events.get_event_by_id(event_id) do
      {:ok, tickets_sold} = Events.tickets_sold(event)
      remaining_tickets = event.allocation - tickets_sold

      # see https://hexdocs.pm/phoenix_html/Phoenix.HTML.Form.html
      render(conn, "new.html", %{
        changeset: Events.new_ticket_changeset(),
        event: event,
        remaining_tickets: remaining_tickets
      })
    end
  end

  @doc """
  STEP 2: Renders the Stripe payment form
  """
  def edit(conn, %{"id" => ticket_id}) do
    with {:ok, ticket} <- Events.get_ticket_by_id(ticket_id) do
      case ticket.state do
        "purchased" -> conn |> redirect(to: Routes.ticket_path(conn, :show, ticket_id))
        "pending" -> render(conn, "edit.html", %{ticket: ticket})
      end
    end
  end

  @doc """
  STEP 3: Renders the confirmation / receipt / thank you screen
  """
  def show(conn, %{"id" => ticket_id}) do
    with {:ok, ticket} <- Events.get_ticket_by_id(ticket_id) do
      render(conn, "show.html", %{ticket: ticket})
    end
  end

  # TRANSITIONS BETWEEN WIZARD STEPS

  @doc """
  Reserves a ticket for 5 minutes
  """
  def create(conn, %{"event_id" => event_id, "ticket" => %{"user_name" => user_name}}) do
    with {:ok, event} <- Events.get_event_by_id(event_id) do
      case Events.reserve_ticket(event, user_name) do
        {:ok, ticket} ->
          Logger.info(
            "New ticket created, id: #{inspect(ticket.id)}, user_name: #{
              inspect(ticket.user_name)
            }"
          )

          conn |> redirect(to: Routes.ticket_path(conn, :edit, ticket.id))

        _error ->
          conn
          |> put_flash(:info, "All tickets sold.")
          |> redirect(to: Routes.event_path(conn, :show, event.id))
      end
    end
  end
end
