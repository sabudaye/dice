defmodule NaiveDiceWeb.GuestController do
  use NaiveDiceWeb, :controller

  alias NaiveDice.Events
  @doc """
  Lists all the guests in the system, across all events (we have just 1 event in seeds.exs)
  Since we only sell 1 ticket per person, just render the list of tickets
  """
  @spec index(Plug.Conn.t(), any) :: Plug.Conn.t()
  def index(conn, %{"event_id" => event_id}) do
    with {:ok, event} <- Events.get_event_by_id(event_id) do
      {:ok, guest_names} = Events.all_guests(event)

      render(conn, "index.html", %{guest_names: guest_names, event: event})
    end
  end

  @doc """
  Helper method which returns the system into the original state (useful for testing)
  """
  @spec reset_guests(Plug.Conn.t(), any) :: Plug.Conn.t()
  def reset_guests(conn, %{"event_id" => event_id}) do
    with {:ok, event} <- Events.get_event_by_id(event_id) do
      Events.delete_all_tickets(event)

      conn
      |> put_flash(:info, "All tickets deleted. Starting from scratch.")
      |> redirect(to: "/")
    end
  end
end
