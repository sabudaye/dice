defmodule NaiveDiceWeb.PaymentController do
  use NaiveDiceWeb, :controller

  require Logger

  @publishable_key Application.get_env(:naive_dice, :publishable_key)
  @wh_secret Application.get_env(:naive_dice, :wh_secret)

  @doc """
  Action used to create intent from client side
  """
  @spec intent(Plug.Conn.t(), any) :: Plug.Conn.t()
  def intent(conn, params) do
    with {:ok, ticket_id} <- Ecto.UUID.cast(params["id"]),
         {:ok, ticket} <- NaiveDice.Events.get_ticket_by_id(ticket_id),
         {:ok, event} <- NaiveDice.Events.get_event_by_id(ticket.event_id) do
      {:ok, intent} =
        Stripe.PaymentIntent.create(%{
          # in cents
          amount: event.price,
          currency: "USD"
        })

      {:ok, ticket} = NaiveDice.Events.set_ticket_intent(ticket, intent)

      Logger.info(
        "Set payment intent to ticket id: #{ticket.id}, user_name: #{ticket.user_name}, intent: #{
          intent.id
        }"
      )

      json(conn, %{
        id: intent.id,
        clientSecret: intent.client_secret,
        publishableKey: @publishable_key
      })
    end
  end

  def webhook(conn, params) do
    type = params["type"]
    id = params["id"]
    Logger.info("Webhook #{inspect(type)} received, id: #{inspect(id)}")

    [signature_header] = get_req_header(conn, "stripe-signature")
    raw_body = NaiveDiceWeb.CacheBodyReader.read_cached_body(conn)

    case Stripe.Webhook.construct_event(raw_body, signature_header, @wh_secret) do
      {:ok, %Stripe.Event{type: "charge.succeeded"} = event} ->
        intent = event.data.object.payment_intent
        {:ok, ticket} = NaiveDice.Events.ticket_purchased(intent)

        Logger.info(
          "Payment suceeded, ticket_id: #{ticket.id}, user_name: #{ticket.user_name}, intent: #{
            intent
          }"
        )

        json(conn, %{status: "success"})

      {:error, reason} ->
        Logger.warn(
          "Webhook #{inspect(type)} failed, id: #{inspect(id)}, reason: #{inspect(reason)}"
        )

        conn
        |> put_status(400)
        |> json(%{status: "failure"})

      unhandled_event ->
        Logger.warn("Unexpected webhook received: #{inspect(unhandled_event)}")

        json(conn, %{status: "success"})
    end
  end
end
