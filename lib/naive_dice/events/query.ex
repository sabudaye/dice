defmodule NaiveDice.Events.Query do
  @moduledoc """
  Module which contains helper functions to create queries for events and ticekts data
  """
  use Timex

  import Ecto.Query
  alias NaiveDice.Events.Ticket

  @spec tickets_sold(pos_integer()) :: Ecto.Query.t()
  def tickets_sold(event_id) do
    timeout = Timex.shift(Timex.now(), minutes: -5)

    from(t in Ticket,
      where: t.event_id == ^event_id and t.state == "pending" and t.updated_at >= ^timeout,
      or_where: t.event_id == ^event_id and t.state == "purchased",
      select: count(t.id)
    )
  end

  @spec first_available_ticket(pos_integer()) :: Ecto.Query.t()
  def first_available_ticket(event_id) do
    timeout = Timex.shift(Timex.now(), minutes: -5)

    from(t in Ticket,
      where: t.event_id == ^event_id,
      where: t.state == "pending",
      where: t.updated_at <= ^timeout,
      limit: 1
    )
  end

  @spec all_guests(pos_integer()) :: Ecto.Query.t()
  def all_guests(event_id) do
    from(t in Ticket,
      where: t.event_id == ^event_id,
      select: t.user_name
    )
  end

  @spec all_tickets(pos_integer()) :: Ecto.Query.t()
  def all_tickets(event_id) do
    from(t in Ticket, where: t.event_id == ^event_id)
  end

  @spec ticket_by_intent(String.t()) :: Ecto.Query.t()
  def ticket_by_intent(intent) do
    from(t in Ticket,
      where: t.intent == ^intent
    )
  end
end
