defmodule NaiveDice.Events.Event do
  @moduledoc "Event schema"

  use Ecto.Schema

  schema "events" do
    field :allocation, :integer
    field :title
    field :price, :integer

    timestamps()
  end
end
