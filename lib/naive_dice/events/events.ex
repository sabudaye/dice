defmodule NaiveDice.Events do
  @moduledoc "Events context"

  alias NaiveDice.Events.{Ticket, Event, Query}
  alias NaiveDice.Repo

  @spec list_all :: list(%Event{})
  def list_all do
    Repo.all(Event)
  end

  @spec get_event_by_id(any) :: {:error, :not_found} | {:ok, %Event{}}
  def get_event_by_id(id) do
    case Repo.get(Event, id) do
      nil ->
        {:error, :not_found}

      event ->
        {:ok, event}
    end
  end

  @spec new_ticket_changeset :: Ecto.Changeset.t()
  def new_ticket_changeset do
    Ticket.changeset(%Ticket{})
  end

  @doc """
  Reserves ticket for an event if there are available tickets

  :global.trans locks event in all connected nodes and sequentially creates new tickets in db
  (or changes user_name in existing expired ticket)
  """
  @spec reserve_ticket(any, String.t()) ::
          {:ok, %Ticket{}} | {:error, :sold_out} | {:error, Ecto.Changeset.t()}
  def reserve_ticket(event, user_name) do
    :global.trans({{:reserve_ticket, event.id}, self()}, fn ->
      with {:ok, :new} <- first_available_ticket(event),
           true <- enough_tickets?(event) do
        create_new_ticket(event, user_name)
      else
        {:ok, ticket} -> change_ticket_owner(ticket, user_name)
        _ -> {:error, :sold_out}
      end
    end)
  end

  @spec tickets_sold(%Event{}) :: {:ok, pos_integer()}
  def tickets_sold(event) do
    query = Query.tickets_sold(event.id)

    case Repo.one(query) do
      nil -> {:ok, 0}
      result -> {:ok, result}
    end
  end

  @spec first_available_ticket(%Event{}) :: {:ok, :new} | {:ok, %Ticket{}}
  def first_available_ticket(event) do
    query = Query.first_available_ticket(event.id)

    case Repo.one(query) do
      nil -> {:ok, :new}
      ticket -> {:ok, ticket}
    end
  end

  @spec change_ticket_owner(%Ticket{}, String.t()) :: {:ok, %Ticket{}}
  def change_ticket_owner(ticket, user_name) do
    ticket
    |> Ticket.changeset(%{user_name: user_name})
    |> Repo.update()
  end

  @spec create_new_ticket(%Event{}, String.t()) :: {:ok, %Ticket{}} | {:error, Ecto.Changeset.t()}
  def create_new_ticket(event, user_name) do
    %Ticket{}
    |> Ticket.changeset(%{event_id: event.id, user_name: user_name, state: "pending"})
    |> Repo.insert()
  end

  @spec get_ticket_by_id(any) :: {:error, :not_found} | {:ok, %Ticket{}}
  def get_ticket_by_id(ticket_id) do
    case Repo.get(Ticket, ticket_id) do
      nil ->
        {:error, :not_found}

      ticket ->
        {:ok, ticket}
    end
  end

  @spec all_guests(%Event{}) :: {:ok, list(String.t())}
  def all_guests(event) do
    result =
      event.id
      |> Query.all_guests()
      |> Repo.all()

    {:ok, result}
  end

  @spec delete_all_tickets(%Event{}) :: {:ok, :removed}
  def delete_all_tickets(event) do
    event.id
    |> Query.all_tickets()
    |> Repo.delete_all()

    {:ok, :removed}
  end

  @spec set_ticket_intent(%Ticket{}, Stripe.PaymentIntent.t()) :: {:ok, %Ticket{}}
  def set_ticket_intent(ticket, intent) do
    ticket
    |> Ticket.changeset(%{intent: intent.id})
    |> Repo.update()
  end

  @spec ticket_purchased(String.t()) :: {:ok, %Ticket{}} | {:error, :not_found}
  def ticket_purchased(intent_id) do
    query = Query.ticket_by_intent(intent_id)

    case Repo.one(query) do
      nil -> {:error, :not_found}
      ticket ->
        ticket
        |> Ticket.changeset(%{state: "purchased"})
        |> Repo.update()
    end
  end

  defp enough_tickets?(event) do
    {:ok, tickets_sold} = tickets_sold(event)
    event.allocation >= tickets_sold + 1
  end
end
