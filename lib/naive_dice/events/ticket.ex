defmodule NaiveDice.Events.Ticket do
  @moduledoc """
  This schema represents a purchased ticket

  TODO: should we use the same schema to represent a pending reservation?
  """

  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}

  schema "tickets" do
    field :user_name, :string
    field :event_id, :integer
    # pending, purchased
    field :state, :string
    field :intent, :string

    timestamps()
  end

  @doc false
  def changeset(ticket, attrs \\ %{}) do
    ticket
    |> cast(attrs, [:user_name, :event_id, :state, :intent])
    |> validate_required([:user_name, :event_id])
    |> unique_constraint(:user_name,
      name: :tickets_event_id_user_name,
      message: "ticket already exists"
    )
  end
end
